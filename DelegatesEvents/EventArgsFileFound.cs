﻿using System;

namespace DelegatesEvents
{
    public class EventArgsFileFound : EventArgs
    {
        public EventArgsFileFound(string fileName)
        {
            FileName = fileName;
        }
        public string FileName { get; }
        
    }
}
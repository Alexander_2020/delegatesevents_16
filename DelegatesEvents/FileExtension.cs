﻿using System;
using System.IO;
using System.Linq;

namespace DelegatesEvents
{
    public class FileExtension
    {
        private readonly string _directoryPath = $"{Environment.CurrentDirectory}\\TestDir\\";
        
        public string[] Files => Directory.GetFiles(_directoryPath);
        public bool StopFind { get; set; }

        public event EventHandler<EventArgsFileFound> FileFound;

        public FileExtension()
        {
            if (Directory.Exists(_directoryPath))
            {
                if (!Directory.GetFiles(_directoryPath).Any())
                    CreateFiles(_directoryPath);
            }
            else
            {
                Directory.CreateDirectory(_directoryPath);
                CreateFiles(_directoryPath);    
            }
        }

        public void FindFile()
        {
            StopFind = false;
            
            foreach (var item in Directory.GetFiles(_directoryPath))
            {
                if (StopFind) break;
                FileFound?.Invoke(this, new EventArgsFileFound(item));
            }
        }

        private void CreateFiles(string directory)
        {
            for (int i = 0; i < 101; i++)
                File.Create($"{directory}{i.ToString()}.txt");
        }
    }
}
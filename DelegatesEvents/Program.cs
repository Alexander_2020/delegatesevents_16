﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DelegatesEvents
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            FileExtension fileExtension = new FileExtension();
            
            //подписка для получения имен файлов при обходе
            fileExtension.FileFound += (sender, eventArgs) =>
            {
                //вывод файлов в консоль и прерывание обхода
                if (eventArgs.FileName.Contains("99")) fileExtension.StopFind = true;
                Console.WriteLine(eventArgs.FileName);
            };
            
            fileExtension.FindFile();

            //поиск максимального и вывод в консоль
            Console.WriteLine(fileExtension.Files.GetMax(s => s.Length));
        }
        
        
    }

    public static class IEnumerable
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, int> getParameter) where T : class
        {
            if (getParameter == null) throw new NullReferenceException("getParameter must not be null");
            
            int max = 0;
            T maxT = null;
            
            foreach (var item in e)
            {
                var tmp = getParameter.Invoke(item);
                if (tmp > max)
                {
                    max = tmp;
                    maxT = item;
                }

            }

            return maxT;
        }
    }
}